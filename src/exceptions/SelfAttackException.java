package exceptions;

public class SelfAttackException extends Exception {
	public SelfAttackException(String message) {
		super(message);
	}
}
