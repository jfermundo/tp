package exceptions;

public class NotYourPieceException extends Exception {
	public NotYourPieceException(String message) {
		super(message);
	}
}
