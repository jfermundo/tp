package exceptions;

public class EmptySquareException extends Exception {
	public EmptySquareException(String message) {
		super(message);
	}
}
