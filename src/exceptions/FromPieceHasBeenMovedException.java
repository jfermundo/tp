package exceptions;

public class FromPieceHasBeenMovedException extends Exception {
	public FromPieceHasBeenMovedException(String message) {
		super(message);
	}
}
