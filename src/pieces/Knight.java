package pieces;

import java.util.ArrayList;

import main.MoveType;
import move_effects.CaptureToPieceIfAny;
import move_requirements.MoveRequirement;

public class Knight extends Piece {

	public Knight(boolean isWhite) {
		super(isWhite);
		
		MoveType moveType;
		ArrayList<MoveRequirement> requirements;
		
		requirements = new ArrayList<MoveRequirement>();
		moveType = new MoveType();
		moveType.setDirection("NNE");
		moveType.setDistance(3); // no lo usamos realmente para el caballero
		moveType.setRequirements(requirements);
		moveType.setEffect(new CaptureToPieceIfAny());
		moveTypes.add(moveType);
		
		requirements = new ArrayList<MoveRequirement>();
		moveType = new MoveType();
		moveType.setDirection("NNW");
		moveType.setDistance(3); // no lo usamos realmente para el caballero
		moveType.setRequirements(requirements);
		moveType.setEffect(new CaptureToPieceIfAny());
		moveTypes.add(moveType);
		
		requirements = new ArrayList<MoveRequirement>();
		moveType = new MoveType();
		moveType.setDirection("NEE");
		moveType.setDistance(3); // no lo usamos realmente para el caballero
		moveType.setRequirements(requirements);
		moveType.setEffect(new CaptureToPieceIfAny());
		moveTypes.add(moveType);
		
		requirements = new ArrayList<MoveRequirement>();
		moveType = new MoveType();
		moveType.setDirection("NWW");
		moveType.setDistance(3); // no lo usamos realmente para el caballero
		moveType.setRequirements(requirements);
		moveType.setEffect(new CaptureToPieceIfAny());
		moveTypes.add(moveType);
		
		requirements = new ArrayList<MoveRequirement>();
		moveType = new MoveType();
		moveType.setDirection("SSE");
		moveType.setDistance(3); // no lo usamos realmente para el caballero
		moveType.setRequirements(requirements);
		moveType.setEffect(new CaptureToPieceIfAny());
		moveTypes.add(moveType);
		
		requirements = new ArrayList<MoveRequirement>();
		moveType = new MoveType();
		moveType.setDirection("SSW");
		moveType.setDistance(3); // no lo usamos realmente para el caballero
		moveType.setRequirements(requirements);
		moveType.setEffect(new CaptureToPieceIfAny());
		moveTypes.add(moveType);
		
		requirements = new ArrayList<MoveRequirement>();
		moveType = new MoveType();
		moveType.setDirection("SEE");
		moveType.setDistance(3); // no lo usamos realmente para el caballero
		moveType.setRequirements(requirements);
		moveType.setEffect(new CaptureToPieceIfAny());
		moveTypes.add(moveType);
		
		requirements = new ArrayList<MoveRequirement>();
		moveType = new MoveType();
		moveType.setDirection("SWW");
		moveType.setDistance(3); // no lo usamos realmente para el caballero
		moveType.setRequirements(requirements);
		moveType.setEffect(new CaptureToPieceIfAny());
		moveTypes.add(moveType);
	}

	public String getAsString() {
		return getColorAsString() + "Kn";
	}
}
