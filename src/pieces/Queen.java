package pieces;

import java.util.ArrayList;

import main.MoveType;
import move_effects.CaptureToPieceIfAny;
import move_requirements.MoveRequirement;
import move_requirements.PathIsClear;

public class Queen extends Piece {

	public Queen(boolean isWhite) {
		super(isWhite);
		
		MoveType moveType;
		ArrayList<MoveRequirement> requirements;
		
		requirements = new ArrayList<MoveRequirement>();
		requirements.add(new PathIsClear());
		moveType = new MoveType();
		moveType.setDirection("N");
		moveType.setDistance(0);
		moveType.setRequirements(requirements);
		moveType.setEffect(new CaptureToPieceIfAny());
		moveTypes.add(moveType);
		
		requirements = new ArrayList<MoveRequirement>();
		requirements.add(new PathIsClear());
		moveType = new MoveType();
		moveType.setDirection("S");
		moveType.setDistance(0);
		moveType.setRequirements(requirements);
		moveType.setEffect(new CaptureToPieceIfAny());
		moveTypes.add(moveType);
		
		requirements = new ArrayList<MoveRequirement>();
		requirements.add(new PathIsClear());
		moveType = new MoveType();
		moveType.setDirection("E");
		moveType.setDistance(0);
		moveType.setRequirements(requirements);
		moveType.setEffect(new CaptureToPieceIfAny());
		moveTypes.add(moveType);
		
		requirements = new ArrayList<MoveRequirement>();
		requirements.add(new PathIsClear());
		moveType = new MoveType();
		moveType.setDirection("W");
		moveType.setDistance(0);
		moveType.setRequirements(requirements);
		moveType.setEffect(new CaptureToPieceIfAny());
		moveTypes.add(moveType);
		
		requirements = new ArrayList<MoveRequirement>();
		requirements.add(new PathIsClear());
		moveType = new MoveType();
		moveType.setDirection("NE");
		moveType.setDistance(0);
		moveType.setRequirements(requirements);
		moveType.setEffect(new CaptureToPieceIfAny());
		moveTypes.add(moveType);
		
		requirements = new ArrayList<MoveRequirement>();
		requirements.add(new PathIsClear());
		moveType = new MoveType();
		moveType.setDirection("NW");
		moveType.setDistance(0);
		moveType.setRequirements(requirements);
		moveType.setEffect(new CaptureToPieceIfAny());
		moveTypes.add(moveType);
		
		requirements = new ArrayList<MoveRequirement>();
		requirements.add(new PathIsClear());
		moveType = new MoveType();
		moveType.setDirection("SE");
		moveType.setDistance(0);
		moveType.setRequirements(requirements);
		moveType.setEffect(new CaptureToPieceIfAny());
		moveTypes.add(moveType);
		
		requirements = new ArrayList<MoveRequirement>();
		requirements.add(new PathIsClear());
		moveType = new MoveType();
		moveType.setDirection("SW");
		moveType.setDistance(0);
		moveType.setRequirements(requirements);
		moveType.setEffect(new CaptureToPieceIfAny());
		moveTypes.add(moveType);
	}

	public String getAsString() {
		return getColorAsString() + "Q ";
	}
}
