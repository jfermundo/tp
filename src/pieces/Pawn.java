package pieces;

import java.util.ArrayList;

import main.MoveType;
import move_effects.CapturePiece;
import move_effects.CaptureToPieceIfAny;
import move_effects.FlagAsHasJumped;
import move_requirements.MoveRequirement;
import move_requirements.PathIsClear;
import move_requirements.PieceHasNotBeenMoved;
import move_requirements.PieceIsPawnAndHasJumped;
import move_requirements.SquareIsEmpty;
import move_requirements.PieceIsEnemy;

public class Pawn extends Piece {
	private boolean hasJumped;

	public Pawn(boolean isWhite) {
		super(isWhite);
		String direction, direction2, direction3, direction4;
		if(isWhite) {
			direction  = "N";
			direction2 = "NE";
			direction3 = "NW";
			direction4 = "S";
		} else {
			direction  = "S";
			direction2 = "SE";
			direction3 = "SW";
			direction4 = "N";
		}
		
		MoveType moveType;
		ArrayList<MoveRequirement> requirements;
		
		requirements = new ArrayList<MoveRequirement>();
		requirements.add(new SquareIsEmpty());
		moveType = new MoveType();
		moveType.setDirection(direction);
		moveType.setDistance(1);
		moveType.setRequirements(requirements);
		moveTypes.add(moveType);
		
		// Salto
		requirements = new ArrayList<MoveRequirement>();
		requirements.add(new SquareIsEmpty());
		requirements.add(new PathIsClear());
		requirements.add(new PieceHasNotBeenMoved(this));
		moveType = new MoveType();
		moveType.setDirection(direction);
		moveType.setDistance(2);
		moveType.setRequirements(requirements);
		moveType.setEffect(new FlagAsHasJumped(this));
		moveTypes.add(moveType);
		
		// Captura este
		requirements = new ArrayList<MoveRequirement>();
		requirements.add(new PieceIsEnemy(isWhite));
		moveType = new MoveType();
		moveType.setDirection(direction2);
		moveType.setDistance(1);
		moveType.setRequirements(requirements);
		moveType.setEffect(new CaptureToPieceIfAny());
		moveTypes.add(moveType);
		
		// Captura oeste
		requirements = new ArrayList<MoveRequirement>();
		requirements.add(new PieceIsEnemy(isWhite));
		moveType = new MoveType();
		moveType.setDirection(direction3);
		moveType.setDistance(1);
		moveType.setRequirements(requirements);
		moveType.setEffect(new CaptureToPieceIfAny());
		moveTypes.add(moveType);
		
		// En passant este
		requirements = new ArrayList<MoveRequirement>();
		requirements.add(new SquareIsEmpty());
		requirements.add(new PieceIsEnemy(isWhite, direction4, 1));
		requirements.add(new PieceIsPawnAndHasJumped(isWhite, direction4, 1));
		moveType = new MoveType();
		moveType.setDirection(direction2);
		moveType.setDistance(1);
		moveType.setRequirements(requirements);
		moveType.setEffect(new CapturePiece(direction4, 1));
		moveTypes.add(moveType);
		
		// En passant oeste
		requirements = new ArrayList<MoveRequirement>();
		requirements.add(new SquareIsEmpty());
		requirements.add(new PieceIsEnemy(isWhite, direction4, 1));
		requirements.add(new PieceIsPawnAndHasJumped(isWhite, direction4, 1));
		moveType = new MoveType();
		moveType.setDirection(direction3);
		moveType.setDistance(1);
		moveType.setRequirements(requirements);
		moveType.setEffect(new CapturePiece(direction4, 1));
		moveTypes.add(moveType);
	}

	public void setHasJumped(boolean hasJumped) {
		this.hasJumped = hasJumped;
	}

	public boolean getHasJumped() {
		return hasJumped;
	}

	public String getAsString() {
		return getColorAsString() + "P ";
	}

}
