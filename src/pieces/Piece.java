package pieces;

import java.util.ArrayList;

import main.MoveType;

public abstract class Piece {
	protected ArrayList<MoveType> moveTypes;
	private boolean isWhite;
	private boolean hasBeenMoved;
	
	public Piece(boolean isWhite) {
		this.isWhite = isWhite;
		moveTypes = new ArrayList<MoveType>();
		hasBeenMoved = false;
	}
	
	public ArrayList<MoveType> getMoveTypes() {
		return moveTypes;
	}
	
	public boolean getIsWhite() {
		return isWhite;
	}
	
	public boolean getHasBeenMoved() {
		return hasBeenMoved;
	}
	
	public void setHasBeenMoved(boolean hasBeenMoved) {
		this.hasBeenMoved = hasBeenMoved;
	}

	protected String getColorAsString() {
		return isWhite ? "w" : "b";
	}
	
	abstract public String getAsString();
}
