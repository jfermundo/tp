package pieces;

import java.util.ArrayList;

import main.MoveType;
import move_effects.CaptureToPieceIfAny;
import move_effects.MovePiece;
import move_requirements.MoveRequirement;
import move_requirements.PathIsClear;
import move_requirements.PieceHasNotBeenMoved;
import move_requirements.SquareIsEmpty;

public class King extends Piece {

	public King(boolean isWhite) {
		super(isWhite);
		
		MoveType moveType;
		ArrayList<MoveRequirement> requirements;
		
		requirements = new ArrayList<MoveRequirement>();
		moveType = new MoveType();
		moveType.setDirection("N");
		moveType.setDistance(1);
		moveType.setRequirements(requirements);
		moveType.setEffect(new CaptureToPieceIfAny());
		moveTypes.add(moveType);
		
		requirements = new ArrayList<MoveRequirement>();
		moveType = new MoveType();
		moveType.setDirection("S");
		moveType.setDistance(1);
		moveType.setRequirements(requirements);
		moveType.setEffect(new CaptureToPieceIfAny());
		moveTypes.add(moveType);
		
		requirements = new ArrayList<MoveRequirement>();
		moveType = new MoveType();
		moveType.setDirection("E");
		moveType.setDistance(1);
		moveType.setRequirements(requirements);
		moveType.setEffect(new CaptureToPieceIfAny());
		moveTypes.add(moveType);
		
		requirements = new ArrayList<MoveRequirement>();
		moveType = new MoveType();
		moveType.setDirection("W");
		moveType.setDistance(1);
		moveType.setRequirements(requirements);
		moveType.setEffect(new CaptureToPieceIfAny());
		moveTypes.add(moveType);
		
		requirements = new ArrayList<MoveRequirement>();
		moveType = new MoveType();
		moveType.setDirection("NE");
		moveType.setDistance(1);
		moveType.setRequirements(requirements);
		moveType.setEffect(new CaptureToPieceIfAny());
		moveTypes.add(moveType);
		
		requirements = new ArrayList<MoveRequirement>();
		moveType = new MoveType();
		moveType.setDirection("NW");
		moveType.setDistance(1);
		moveType.setRequirements(requirements);
		moveType.setEffect(new CaptureToPieceIfAny());
		moveTypes.add(moveType);
		
		requirements = new ArrayList<MoveRequirement>();
		moveType = new MoveType();
		moveType.setDirection("SE");
		moveType.setDistance(1);
		moveType.setRequirements(requirements);
		moveType.setEffect(new CaptureToPieceIfAny());
		moveTypes.add(moveType);
		
		requirements = new ArrayList<MoveRequirement>();
		moveType = new MoveType();
		moveType.setDirection("SW");
		moveType.setDistance(1);
		moveType.setRequirements(requirements);
		moveType.setEffect(new CaptureToPieceIfAny());
		moveTypes.add(moveType);
		
		// Enroque corto
		requirements = new ArrayList<MoveRequirement>();
		requirements.add(new PathIsClear());
		requirements.add(new SquareIsEmpty());
		requirements.add(new PieceHasNotBeenMoved(this));
		requirements.add(new PieceHasNotBeenMoved(isWhite ? "H1" : "H8"));
		moveType = new MoveType();
		moveType.setDirection("E");
		moveType.setDistance(2);
		moveType.setRequirements(requirements);
		moveType.setEffect(new MovePiece(isWhite ? "H1" : "H8", isWhite ? "F1" : "F8"));
		moveTypes.add(moveType);
		
		// Enroque largo
		requirements = new ArrayList<MoveRequirement>();
		requirements.add(new PathIsClear());
		requirements.add(new SquareIsEmpty());
		requirements.add(new SquareIsEmpty(isWhite ? "B1" : "B8"));
		requirements.add(new PieceHasNotBeenMoved(this));
		requirements.add(new PieceHasNotBeenMoved(isWhite ? "A1" : "A8"));
		moveType = new MoveType();
		moveType.setDirection("W");
		moveType.setDistance(2);
		moveType.setRequirements(requirements);
		moveType.setEffect(new MovePiece(isWhite ? "A1" : "A8", isWhite ? "D1" : "D8"));
		moveTypes.add(moveType);
	}
	
	public String getAsString() {
		return getColorAsString() + "Ki";
	}
}
