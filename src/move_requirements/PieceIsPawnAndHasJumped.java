package move_requirements;

import pieces.Pawn;
import pieces.Piece;
import main.Board;
import main.Debugger;
import main.Move;

public class PieceIsPawnAndHasJumped implements MoveRequirement {
	private boolean isWhite;
	private String direction;
	private int distance;
	
	public PieceIsPawnAndHasJumped(boolean isWhite, String direction, int distance) {
		this.isWhite = isWhite;
		this.direction = direction;
		this.distance = distance;
	}
	
	public boolean check(Move move, Board board) {
		Piece piece = board.getPiece(move.getTo(), direction, distance);
		Debugger.debug("to: " + move.getTo());
		Debugger.debug("direction: " + direction);
		Debugger.debug("distance: " + distance);
		Debugger.debug("null: " + (piece != null ? "bien" : "mal"));
		Debugger.debug("last piece moved: " + (board.getLastPieceMoved() == piece ? "bien" : "mal"));
		Debugger.debug("class: " + (piece.getClass().getName().equals("Pawn") ? "bien" : "mal"));
		Debugger.debug("class: " + (((Pawn) piece).getHasJumped() ? "bien" : "mal"));
		return piece != null
			&& board.getLastPieceMoved() == piece
			&& piece.getClass().getName().equals("Pawn")
			&& ((Pawn) piece).getHasJumped()
		;
	}

	public String getMessage() {
		return "Debe haber un peón enemigo al " + (isWhite ? "sur" : "norte") + " del casillero destino, y debe haber avanzado dos casilleros en la jugada anterior.";
	}
}
