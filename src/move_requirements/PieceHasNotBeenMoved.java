package move_requirements;

import pieces.Piece;
import main.Board;
import main.Move;

public class PieceHasNotBeenMoved implements MoveRequirement {
	private Piece piece;
	private String squareName;
	
	public PieceHasNotBeenMoved(Piece piece) {
		this.piece = piece;
	}
	
	public PieceHasNotBeenMoved(String squareName) {
		this.squareName = squareName;
	}

	public boolean check(Move move, Board board) {
		if(squareName != null) {
			piece = board.getPiece(squareName);
		}
		return piece != null && ! piece.getHasBeenMoved();
	}

	public String getMessage() {
		return "Para poder realizar esta jugada, la pieza debe no haber sido movida ni capturada.";
	}
}
