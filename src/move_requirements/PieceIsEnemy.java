package move_requirements;

import pieces.Piece;
import main.Board;
import main.Debugger;
import main.Move;

public class PieceIsEnemy implements MoveRequirement {
	private boolean isWhite;
	private String direction;
	private int distance;
	
	public PieceIsEnemy(boolean isWhite) {
		this.isWhite = isWhite;
	}
	
	public PieceIsEnemy(boolean isWhite, String direction, int distance) {
		this.isWhite = isWhite;
		this.direction = direction;
		this.distance = distance;
	}
	
	public boolean check(Move move, Board board) {
		Piece piece;
		if(direction != null) {
			piece = board.getPiece(move.getTo(), direction, distance);
		} else {
			piece = board.getPiece(move.getTo());
		}
		Debugger.debug("to: " + move.getTo());
		Debugger.debug("direction: " + direction);
		Debugger.debug("distance: " + distance);
		Debugger.debug("null: " + (piece != null ? "bien" : "mal"));
		if(piece != null) { Debugger.debug("white: " + (piece.getIsWhite() != isWhite ? "bien" : "mal")); }
		return piece != null && piece.getIsWhite() != isWhite;
	}

	public String getMessage() {
		return "Debe haber una pieza enemiga en el casillero.";
	}
}
