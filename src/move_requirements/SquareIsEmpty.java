package move_requirements;

import pieces.Piece;
import main.Board;
import main.Move;

public class SquareIsEmpty implements MoveRequirement {
	private String squareName;
	private String direction;
	private int distance;
	
	public SquareIsEmpty() {}
	
	public SquareIsEmpty(String squareName) {
		this.squareName = squareName;
	}
	
	public SquareIsEmpty(String direction, int distance) {
		this.direction = direction;
		this.distance = distance;
	}

	public boolean check(Move move, Board board) {
		Piece piece;
		if(direction != null) {
			piece = board.getPiece(move.getTo(), direction, distance);
		} else if(squareName != null) {
			piece = board.getPiece(squareName);
		} else {
			piece = board.getPiece(move.getTo());
		}
		if(piece == null) {
			return true;
		}
		return false;
	}

	public String getMessage() {
		return "Ese casillero está ocupado.";
	}
}
