package move_requirements;

import main.Board;
import main.Move;

public interface MoveRequirement {
	public boolean check(Move move, Board board);
	public String getMessage();
}
