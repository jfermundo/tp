package user_interface;

import java.util.Observable;

import main.Board;
import main.Move;

public abstract class UserInterface extends Observable {
	private Board board;
	
	abstract public Move inputMove(boolean isWhite);
	
	abstract public void startGame();

	abstract public void invalidMoveError();
	
	abstract public void emptySquareError();

	abstract public void notYourPieceError();

	public abstract void nextTurn(boolean white);
	
	
	public Board getBoard() {
		return board;
	}
	public void setBoard(Board board) {
		this.board = board;
	}

	abstract public void selfAttackError();
}
