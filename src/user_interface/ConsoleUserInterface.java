package user_interface;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import pieces.Piece;

import events.StartGameEvent;
import events.MoveEvent;
import main.Board;
import main.Move;

public class ConsoleUserInterface extends UserInterface {
	public Move inputMove(boolean isWhite) {
		if(isWhite) {
			System.out.println("Juegan las blancas: ");
		} else {
			System.out.println("Juegan las negras: ");
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String input = null;
		try {
			input = br.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		input = input.toUpperCase();
		String[] squares = input.split(" ");
		if(squares.length != 2) {
			System.out.println("Debe ingresar origen y destino separado por un espacio. Ej: e2 e4");
			return inputMove(isWhite);
		}
		if(squares[0].length() > 2 || squares[1].length() > 2) {
			System.out.println("Debe ingresar origen y destino separado por un espacio. Ej: e2 e4");
			return inputMove(isWhite);
		}
		if( ! getBoard().isValidSquareName(squares[0]) ||  ! getBoard().isValidSquareName(squares[1])) {
			System.out.println("Casillero inexistente, intente de nuevo.");
			return inputMove(isWhite);
		}
		Move move = new Move(squares[0], squares[1]);
		return move;
	}

	public void startGame() {
		System.out.println("Iniciando el partido...");
		setChanged();
		notifyObservers(new StartGameEvent());
	}
	
	public void nextTurn(boolean isWhite) {
		printBoard();
		Move move = inputMove(isWhite);
		setChanged();
		notifyObservers(new MoveEvent(move, isWhite));
	}
	
	public void invalidMoveError() {
		System.out.println("Movimiento inválido.");
	}
	
	public void emptySquareError() {
		System.out.println("Debe seleccionar una pieza.");
	}

	public void notYourPieceError() {
		System.out.println("Esa pieza no es suya.");
	}
	
	public void selfAttackError() {
		System.out.println("No puede capturar sus propias piezas.");
	}
	
	private void printBoard() {
		Board board = getBoard();
		String line = "________________________________________________";
		String squareName = null;
		String number = "";
		String capturedPiecesString = null;
		int capturedWhitePieces = 0;
		int capturedBlackPieces = 0;
		int i;
		
		for(int y=8; y >= 1; y--) {
			if(number.equals("6")) {
				capturedPiecesString = "       ";
				capturedWhitePieces = 0;
				for(Piece p: getBoard().getCapturedWhitePieces()) {
					capturedPiecesString+= " " + p.getAsString() + " ";
					capturedWhitePieces++;
					if(capturedWhitePieces >= 8) { break; }
				}
			} else if(number.equals("5") && capturedWhitePieces > 8) {
				capturedPiecesString = "       ";
				i = 0;
				for(Piece p: getBoard().getCapturedWhitePieces()) {
					capturedPiecesString+= " " + p.getAsString() + " ";
					i++;
					if(i <= 8) { continue; }
				}
			} else if(number.equals("4")) {
				capturedPiecesString = "       ";
				capturedBlackPieces = 0;
				for(Piece p: getBoard().getCapturedBlackPieces()) {
					capturedPiecesString+= " " + p.getAsString() + " ";
					capturedBlackPieces++;
					if(capturedBlackPieces >= 8) { break; }
				}
			} else if(number.equals("3") && capturedBlackPieces > 8) {
				capturedPiecesString = "       ";
				i = 0;
				for(Piece p: getBoard().getCapturedWhitePieces()) {
					capturedPiecesString+= " " + p.getAsString() + " ";
					i++;
					if(i <= 8) { continue; }
				}
			} else {
				capturedPiecesString = "";
			}
			
			String capturedLine = number.equals("5") ? "      " + line : "";
			System.out.println("\n" + line + capturedLine + capturedPiecesString);
			System.out.print("| ");
			
			for(int x=1; x <= 8; x++) {
				squareName = board.getSquareName(x, y);
				Piece piece = board.getPiece(squareName);
				if(piece instanceof Piece) {
					System.out.print(piece.getAsString());
				} else {
					System.out.print("   ");
				}
				System.out.print(" | ");
			}
			number = squareName.split("")[2];
			System.out.print(number);
		}
		System.out.println("\n" + line);
		
		for(int x=1; x <= 8; x++) {
			squareName = board.getSquareName(x, 1);
			System.out.print("   " + squareName.split("")[1] + "  ");
		}
		System.out.println("\n");
	}
}
