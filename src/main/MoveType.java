package main;

import java.util.ArrayList;

import move_effects.MoveEffect;
import move_requirements.MoveRequirement;

public class MoveType {
	private String direction;
	private int distance;
	private boolean isJump;
	private ArrayList<MoveRequirement> requirements;
	private MoveEffect effect;
	
	public ArrayList<MoveRequirement> getRequirements() {
		return requirements;
	}
	public void setRequirements(ArrayList<MoveRequirement> requirements) {
		this.requirements = requirements;
	}
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}
	public int getDistance() {
		return distance;
	}
	/**
	 * 0 = unlimited
	 * 
	 * @param distance
	 */
	public void setDistance(int distance) {
		this.distance = distance;
	}
	public MoveEffect getEffect() {
		return effect;
	}
	public void setEffect(MoveEffect effect) {
		this.effect = effect;
	}
	public boolean getIsJump() {
		return isJump;
	}
	public void setJump(boolean isJump) {
		this.isJump = isJump;
	}
	
	
	
}
