package main;

import user_interface.ConsoleUserInterface;

/**
 * 
 * @author 65251
 */
public class Main {
	public static void main(String args[]) {
		//Debugger.setDebug(true); /*
		Debugger.setDebug(false); //*/
		//GraphicalUserInterface userInterface = new GraphicalUserInterface(); /*
		ConsoleUserInterface userInterface = new ConsoleUserInterface(); //*/
		Game game = new Game(userInterface);
	}
}
