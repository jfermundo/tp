package main;

import java.util.ArrayList;

import move_effects.MoveEffect;
import move_requirements.MoveRequirement;

import pieces.Bishop;
import pieces.King;
import pieces.Knight;
import pieces.Pawn;
import pieces.Piece;
import pieces.Queen;
import pieces.Rook;
import exceptions.EmptySquareException;
import exceptions.InvalidMoveException;
import exceptions.NotYourPieceException;
import exceptions.SelfAttackException;
import user_interface.UserInterface;

public class Board {
	private Piece[][] matrix;
	private ArrayList<Piece> capturedWhitePieces;
	private ArrayList<Piece> capturedBlackPieces;
	private Piece lastPieceMoved;
	
	public Board() {
		clear();
		setPieces();
		capturedWhitePieces = new ArrayList<Piece>();
		capturedBlackPieces = new ArrayList<Piece>();
	}
	
	public void move(Move move, Player player) throws InvalidMoveException, EmptySquareException, NotYourPieceException, SelfAttackException {
		/**
		 * Validar la jugada
		 */
		
		Piece pieceFrom = getPiece(move.getFrom());
		Piece pieceTo = getPiece(move.getTo());
		
		// Chequear que haya una pieza en from
		if( ! (pieceFrom instanceof Piece)) {
			throw new EmptySquareException("No hay ninguna pieza en el casillero de origen: " + move.getFrom());
		}
		
		// Chequear que la pieza sea del mismo color que el jugador
		if(pieceFrom.getIsWhite() != player.getIsWhite()) {
			throw new NotYourPieceException("La pieza elegida no es suya: " + move.getFrom());
		}
		
		// Chequear que si hay una pieza en To, sea enemiga
		if(pieceTo != null && pieceTo.getIsWhite() == player.getIsWhite()) {
			throw new SelfAttackException("No puede capturar sus propias piezas: " + move.getTo());
		}

		// Buscar en la lista de movimientos posibles de la pieza a ver si alguno coincide
		// con el movimiento que hizo el jugador
		MoveType moveType = findMoveType(move, pieceFrom.getMoveTypes());
		Debugger.debug("found moveType direction: " + moveType.getDirection());
		Debugger.debug("found moveType distance: " + moveType.getDistance());
		
		// Chequear los requerimientos del movimiento que nos da la pieza
		checkMoveRequirements(move, moveType);
		
		// Chequear que el rey no quede en jaque
		
		/**
		 * Realizar la jugada
		 */
		
		MoveEffect effect = moveType.getEffect();
		if(effect != null) { effect.apply(move, this); }
		clearSquare(move.getFrom());
		setPiece(pieceFrom, move.getTo());
		pieceFrom.setHasBeenMoved(true);
		lastPieceMoved = pieceFrom;
	}
	
	private void checkMoveRequirements(Move move, MoveType moveType) throws InvalidMoveException {
		for(MoveRequirement mr: moveType.getRequirements()) {
			if( ! mr.check(move, this)) {
				throw new InvalidMoveException(mr.getMessage());
			}
		}
	}

	private MoveType findMoveType(Move move, ArrayList<MoveType> moveTypes) throws InvalidMoveException {
		Debugger.debug("move direction: " + getMoveDirection(move));
		Debugger.debug("move distance: " + getDistance(move));
		for(MoveType mt: moveTypes) {
			Debugger.debug("moveType direction: " + mt.getDirection());
			if( ! getMoveDirection(move).equals(mt.getDirection())) { continue; }
			Debugger.debug("moveType distance: " + mt.getDistance());
			if(mt.getDistance() != 0 && getDistance(move) != mt.getDistance()) { continue; }
			return mt;
		}
		throw new InvalidMoveException("El movimiento no coincide con ningún movimiento de la pieza.");
	}
	
	private int getDistance(Move move) {
		int[] distances = getDistances(move);
		int distanceX = distances[0];
		int distanceY = distances[1];
		int absDistanceX = Math.abs(distanceX);
		int absDistanceY = Math.abs(distanceY);
		int distance = 0;
		if(distanceX == 0) {
			distance = absDistanceY; // vertical
		} else if(distanceY == 0) {
			distance = absDistanceX; // horizontal
		} else if(absDistanceX == absDistanceY) {
			distance = absDistanceX; // diagonal
		} else if(absDistanceX != absDistanceY) {
			distance = absDistanceX + absDistanceY; // knight, no importa en realidad
		}
		return distance;
	}
	
	private int[] getDistances(Move move) {
		int[] coordinatesFrom = getSquareCoordinates(move.getFrom());
		int[] coordinatesTo = getSquareCoordinates(move.getTo());
		int distanceX = coordinatesTo[0] - coordinatesFrom[0];
		int distanceY = coordinatesTo[1] - coordinatesFrom[1];
		int[] distances = new int[2];
		distances[0] = distanceX;
		distances[1] = distanceY;
		return distances;
	}
	
	private String getMoveDirection(Move move) {
		int[] distances = getDistances(move);
		int distanceX = distances[0];
		int distanceY = distances[1];
		int absDistanceX = Math.abs(distanceX);
		int absDistanceY = Math.abs(distanceY);
		String direction = "";
		
		if(distanceY > 0) {
			direction+= "N";
		} else if(distanceY < 0) {
			direction+= "S";
		}
		if(distanceX > 0) {
			direction+= "E";
		} else if(distanceX < 0) {
			direction+= "W";
		}
		if(distanceX != 0 && absDistanceY > absDistanceX) {
			direction = (distanceY > 0 ? "N" : "S") + direction;
		} else if(distanceY != 0 && absDistanceX > absDistanceY) {
			direction+= distanceX > 0 ? "E" : "W";
		}
		
		return direction;
	}
	
	private void clear() {
		matrix = new Piece[8][8];
	}
	
	public Piece getPiece(String squareName) {
		int coordinates[] = getSquareCoordinates(squareName);
		if(matrix[coordinates[0]] != null && matrix[coordinates[0]][coordinates[1]] != null) {
			return matrix[coordinates[0]][coordinates[1]];
		}
		return null;
	}
	
	public Piece getPiece(String squareName, String direction, int distance) {
		int coordinates[] = getSquareCoordinates(squareName, direction, distance);
		if(matrix[coordinates[0]] != null && matrix[coordinates[0]][coordinates[1]] != null) {
			return matrix[coordinates[0]][coordinates[1]];
		}
		return null;
	}
	
	private void setPieces() {
		boolean isWhite = true;
		setPiece(new Pawn(isWhite),   "A2");
		setPiece(new Pawn(isWhite),   "B2");
		setPiece(new Pawn(isWhite),   "C2");
		setPiece(new Pawn(isWhite),   "D2");
		setPiece(new Pawn(isWhite),   "E2");
		setPiece(new Pawn(isWhite),   "F2");
		setPiece(new Pawn(isWhite),   "G2");
		setPiece(new Pawn(isWhite),   "H2");
		setPiece(new Rook(isWhite),   "A1");
		setPiece(new Knight(isWhite), "B1");
		setPiece(new Bishop(isWhite), "C1");
		setPiece(new Queen(isWhite),  "D1");
		setPiece(new King(isWhite),   "E1");
		setPiece(new Bishop(isWhite), "F1");
		setPiece(new Knight(isWhite), "G1");
		setPiece(new Rook(isWhite),   "H1");
		
		isWhite = false;
		setPiece(new Pawn(isWhite),   "A7");
		setPiece(new Pawn(isWhite),   "B7");
		setPiece(new Pawn(isWhite),   "C7");
		setPiece(new Pawn(isWhite),   "D7");
		setPiece(new Pawn(isWhite),   "E7");
		setPiece(new Pawn(isWhite),   "F7");
		setPiece(new Pawn(isWhite),   "G7");
		setPiece(new Pawn(isWhite),   "H7");
		setPiece(new Rook(isWhite),   "A8");
		setPiece(new Knight(isWhite), "B8");
		setPiece(new Bishop(isWhite), "C8");
		setPiece(new Queen(isWhite),  "D8");
		setPiece(new King(isWhite),   "E8");
		setPiece(new Bishop(isWhite), "F8");
		setPiece(new Knight(isWhite), "G8");
		setPiece(new Rook(isWhite),   "H8");
	}
	
	public void setPiece(Piece piece, String squareName) {
		int[] coordinates = getSquareCoordinates(squareName);
		Debugger.debug("square: " + squareName);
		Debugger.debug("x: " + coordinates[0]);
		Debugger.debug("y: " + coordinates[1]);
		matrix[coordinates[0]][coordinates[1]] = piece;
	}
	
	public void clearSquare(String squareName) {
		int[] coordinates = getSquareCoordinates(squareName);
		matrix[coordinates[0]][coordinates[1]] = null;
	}

	public void capturePiece(String squareName) {
		Piece piece = getPiece(squareName);
		clearSquare(squareName);
		if(piece.getIsWhite()) {
			capturedWhitePieces.add(piece);
		} else {
			capturedBlackPieces.add(piece);
		}
	}
	
	public void capturePiece(String squareName, String direction, int distance) {
		Piece piece = getPiece(squareName, direction, distance);
		clearSquare(squareName);
		if(piece.getIsWhite()) {
			capturedWhitePieces.add(piece);
		} else {
			capturedBlackPieces.add(piece);
		}
	}
	
	public String getSquareName(int x, int y) {
		return "" + (char) (x + 64) + y; // 1 + 64 = 65 = A
	}
	
	public int[] getSquareCoordinates(String squareName) {
		char[] chars = squareName.toCharArray();
		int x = (int) chars[0] - 65; // A = 65 - 65 = 0
		int y = Integer.parseInt("" + chars[1]) - 1; // 0 = 0
		int[] coordinates = { x, y };
		return coordinates;
	}
	
	public int[] getSquareCoordinates(String squareName, String direction, int distance) {
		int[] coordinates = getSquareCoordinates(squareName);
		if(direction.equals("N")) {
			coordinates[1]+= distance;
		} else if(direction.equals("S")) {
			coordinates[1]-= distance;
		} else if(direction.equals("E")) {
			coordinates[0]+= distance;
		} else if(direction.equals("W")) {
			coordinates[0]-= distance;
		} else if(direction.equals("NE")) {
			coordinates[1]+= distance;
			coordinates[0]+= distance;
		} else if(direction.equals("NW")) {
			coordinates[1]+= distance;
			coordinates[0]-= distance;
		} else if(direction.equals("SE")) {
			coordinates[1]-= distance;
			coordinates[0]+= distance;
		} else if(direction.equals("SW")) {
			coordinates[1]-= distance;
			coordinates[0]-= distance;
		}
		return coordinates;
	}

	public boolean isValidSquareName(String string) {
		try {
			int[] coordinates = getSquareCoordinates(string);
			return 0 <= coordinates[0] && coordinates[0] <= 7 && 0 <= coordinates[1] && coordinates[1] <= 7;
		} catch(NumberFormatException e) {
			return false;
		}
	}

	public ArrayList<Piece> getCapturedWhitePieces() {
		return capturedWhitePieces;
	}

	public ArrayList<Piece> getCapturedBlackPieces() {
		return capturedBlackPieces;
	}

	public Piece getLastPieceMoved() {
		return lastPieceMoved;
	}
	
}
