package main;

public class Debugger {
	private static boolean debug;
	
	public static boolean isDebug() {
		return debug;
	}

	public static void setDebug(boolean debug) {
		Debugger.debug = debug;
	}

	public static void debug(String msg) {
		if( ! debug) { return; }
		System.out.println(msg);
	}
	
	public static void debug(Exception e) {
		if( ! debug) { return; }
		e.printStackTrace();
	}
}
