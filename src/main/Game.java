package main;

import java.util.Observable;
import java.util.Observer;
import java.util.Timer;

import user_interface.*;
import events.*;
import exceptions.*;

public class Game implements Observer {
	private Player whitePlayer;
	private Player blackPlayer;
	private Board board;
	private UserInterface userInterface;
	
	public Game(UserInterface userInterface) {
		whitePlayer = new Player(true);
		blackPlayer = new Player(false);
		board = new Board();
		userInterface.setBoard(board);
		this.userInterface = userInterface;
		
		// Subscribir Game a los eventos creados por userInterface
        userInterface.addObserver((Observer) this);
        userInterface.startGame();
	}
	
	public void update(Observable obj, Object event) {
        if(event instanceof StartGameEvent) {
        	nextTurn(true);
        } else if(event instanceof MoveEvent) {
        	MoveEvent e = (MoveEvent) event;
        	move((Move) e.getObject(), e.getIsWhite());
        }
    }
	
	/**
	 * Acá se detiene el algoritmo y quedamos a la espera de eventos
	 * que vengan de la interfaz.
	 * 
	 * @param white
	 */
	private void nextTurn(boolean isWhite) {
		userInterface.nextTurn(isWhite);
	}
	
	private void move(Move move, boolean isWhite) {
		try {
			Player player = isWhite ? whitePlayer : blackPlayer;
			board.move(move, player);
		} catch (InvalidMoveException e) {
			userInterface.invalidMoveError();
			Debugger.debug(e);
			isWhite = ! isWhite;
		} catch (EmptySquareException e) {
			userInterface.emptySquareError();
			Debugger.debug(e);
			isWhite = ! isWhite;
		} catch (NotYourPieceException e) {
			userInterface.notYourPieceError();
			Debugger.debug(e);
			isWhite = ! isWhite;
		} catch (SelfAttackException e) {
			userInterface.selfAttackError();
			Debugger.debug(e);
			isWhite = ! isWhite;
		}
		
		if( ! isOver()) { nextTurn( ! isWhite); }
	}
	
	private boolean isOver() {
		return isCheckMate() /*|| getTimeLeft().toInt() == 0*/;
	}
	
	private boolean isCheckMate() {
		return false;
	}
	
	/*
	private Timer getTimeLeft() {
		return new Timer();
	}
	*/
}
