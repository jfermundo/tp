package move_effects;

import pieces.Piece;
import main.Board;
import main.Move;

public class CaptureToPieceIfAny implements MoveEffect {
	public void apply(Move move, Board board) {
		Piece pieceTo = board.getPiece(move.getTo());
		if(pieceTo instanceof Piece) {
			board.capturePiece(move.getTo());
		}
	}
}
