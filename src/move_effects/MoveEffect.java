package move_effects;

import main.Board;
import main.Move;

public interface MoveEffect {
	public void apply(Move move, Board board);
}
