package move_effects;

import main.Board;
import main.Move;

public class CapturePiece implements MoveEffect {
	private String direction;
	private int distance;
	
	public CapturePiece(String direction, int distance) {
		this.direction = direction;
		this.distance = distance;
	}
	
	public void apply(Move move, Board board) {
		board.capturePiece(move.getTo(), direction, distance);
	}
}
