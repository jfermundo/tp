package move_effects;

import pieces.Piece;
import main.Board;
import main.Move;

public class MovePiece implements MoveEffect {
	private String from;
	private String to;
	
	public MovePiece(String from, String to) {
		this.from = from;
		this.to = to;
	}
	
	public void apply(Move move, Board board) {
		Piece piece = board.getPiece(from);
		board.clearSquare(from);
		board.setPiece(piece, to);
		piece.setHasBeenMoved(true);
	}
}
