package move_effects;

import pieces.Pawn;
import pieces.Piece;
import main.Board;
import main.Move;

public class FlagAsHasJumped implements MoveEffect {
	private Piece piece;
	
	public FlagAsHasJumped(Piece piece) {
		this.piece = piece;
	}
	
	public void apply(Move move, Board board) {
		((Pawn) piece).setHasJumped(true);
	}
}
