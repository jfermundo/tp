package events;

public class Event {
	private Object object;
	
	public Event() {}
	
	public Event(Object object) {
		this.object = object;
	}
	
	public Object getObject() {
		return object;
	}
}
