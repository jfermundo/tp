package events;

public class MoveEvent extends Event {
	private boolean isWhite;
	
	public MoveEvent(Object object, boolean isWhite) {
		super(object);
		this.isWhite = isWhite;
	}

	public boolean getIsWhite() {
		return isWhite;
	}

}
